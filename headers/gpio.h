// =============================================
//        GPIO-���������� ��� STM32F303
//        �����: ������� �.�.
//        ������: v1.00 (��������)
// =============================================

#ifndef __GPIO_H
#define __GPIO_H

// ---------------------------------------------
//             Define-����������
// ---------------------------------------------

// ��������� �����������
#define GPIO_RED_START			__HAL_RCC_GPIOA_CLK_ENABLE()	// ������� ��������� ������������ �������� �����������
#define GPIO_RED_PORT				GPIOA													// ���� � �������� ��������� ������� ���������
#define GPIO_RED_PIN				GPIO_PIN_10										// ��� � �������� ��������� ������� ���������

#define GPIO_GREEN_START		__HAL_RCC_GPIOB_CLK_ENABLE()	// ������� ��������� ������������ �������� �����������
#define GPIO_GREEN_PORT			GPIOB													// ���� � �������� ��������� ������� ���������
#define GPIO_GREEN_PIN			GPIO_PIN_3										// ��� � �������� ��������� ������� ���������

#define GPIO_BLUE_START			__HAL_RCC_GPIOB_CLK_ENABLE()	// ������� ��������� ������������ ������ �����������
#define GPIO_BLUE_PORT			GPIOB													// ���� � �������� ��������� ������ ���������
#define GPIO_BLUE_PIN				GPIO_PIN_5										// ��� � �������� ��������� ������ ���������

// ---------------------------------------------

// ��������� ������
#define GPIO_KEY_A_START		__HAL_RCC_GPIOC_CLK_ENABLE()	// ������� ��������� ������������ ������ A
#define GPIO_KEY_A_PORT			GPIOC													// ���� � �������� ���������� ������ A
#define GPIO_KEY_A_PIN			GPIO_PIN_13										// ��� � �������� ���������� ������ A

#define GPIO_LEFT_START			__HAL_RCC_GPIOB_CLK_ENABLE()	// ������� ��������� ������������ "������ ���"
#define GPIO_LEFT_PORT			GPIOB													// ���� � �������� ��������� "����� ��"
#define GPIO_LEFT_PIN				GPIO_PIN_10										// ��� � �������� ��������� "����� ��"

#define GPIO_RIGHT_START		__HAL_RCC_GPIOC_CLK_ENABLE()	// ������� ��������� ������������ "������� ���"
#define GPIO_RIGHT_PORT			GPIOC													// ���� � �������� ��������� "������ ��"
#define GPIO_RIGHT_PIN			GPIO_PIN_7										// ��� � �������� ��������� "������ ��"


// ---------------------------------------------
// ���-������ (�����������)

// ��������� �������
#define GPIO_TIMER_START		__HAL_RCC_TIM3_CLK_ENABLE()		// ������� ��������� ������������ ������� PWM ��� ����������
#define GPIO_TIMER					TIM3													// ������ ������������ ��� ���������� �������������


// ����� ����������
#define GPIO_LSERVO_START		__HAL_RCC_GPIOA_CLK_ENABLE()	// ������� ��������� ������������ ����� �����������
#define GPIO_LSERVO_PORT		GPIOA													// ���� � �������� ���������� ����� �����������
#define GPIO_LSERVO_PIN			GPIO_PIN_6										// ��� � �������� ���������� ����� �����������
#define GPIO_LSERVO_AF			GPIO_AF2_TIM3									// �������������� ������� - ����� ������� TIM3 (PWM)
#define GPIO_LSERVO_CH			TIM_CHANNEL_1									// ����� ������� ����� ����������� (��� ���������)
#define GPIO_LSERVO_CC			CCR1													// ����� ������� ����� ����������� (��� ������)

// ������ ����������
#define GPIO_RSERVO_START		__HAL_RCC_GPIOA_CLK_ENABLE()	// ������� ��������� ������������ ������ �����������
#define GPIO_RSERVO_PORT		GPIOA													// ���� � �������� ���������� ������ �����������
#define GPIO_RSERVO_PIN			GPIO_PIN_7										// ��� � �������� ���������� ������ �����������
#define GPIO_RSERVO_AF			GPIO_AF2_TIM3									// �������������� ������� - ����� ������� TIM3 (PWM)
#define GPIO_RSERVO_CH			TIM_CHANNEL_2									// ����� ������� ������ ����������� (��� ���������)
#define GPIO_RSERVO_CC			CCR2													// ����� ������� ������ ����������� (��� ������)

// ---------------------------------------------
//         ���������� ������ GPIO
// ---------------------------------------------

class gpio_class
{
	private:
	
	public:
	void init(void);														// ������������� ����� �����-������
	void led(char num, char state);							// ��������� ����������
	void blink(char count, int time);						// �������� ����� ������������ "count" ��� � ������������ "time" ��.
	char key(char num);													// ������ ������
	void servo(char channel, int value);				// ��������� �������� (�� 0 �� 100%) PWM �����������
};

#endif
