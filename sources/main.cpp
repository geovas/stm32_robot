// ================================================================
//                    ����������� ���������
// ================================================================

#include <stdio.h>						// ���������� ������������ �����-������ (� �.�. ������)
#include <cmsis_os.h>					// ���������� ������ � RTOS Keil RTX
#include <stm32f7xx_hal.h>		// �������� ���������� HAL-��������
#include <stm32f7xx.h>				// �������� ���������� STM32F767

#include "gpio.h"							// ���������������� ���������� �����-������ (GPIO)
#include "uart.h"							// ���������������� ���������� ������ � UART

// ================================================================
//                ���������� ���������� � ��������
// ================================================================

extern gpio_class gpio;				// ��������� ������ ��� ������ � GPIO
extern uart_class uart;				// ��������� ������ ��� ������ � UART

// ������ ������, ������������ �������������� ������������ �������
struct __attribute__((packed)) dataStruct
{
	unsigned char head;          // ��������� (�.�. 'D' = 0x44)
	unsigned char increment;     // ��������� ������� (������ ��� �.�. ������)
	unsigned char length;        // ����� ������ (�� device_code �� error, �.�. "�������� ����� - 4" = 0x34)
	unsigned short device_code;   // = 0x1400
	unsigned int device_number;  // �������� ����� ����������
	unsigned short I_leack_code; // = 0x2407
	unsigned int I_leack;        // ��� ������ (���)
	unsigned short I_pulse_code; // = 0x3407
	unsigned int I_pulse;        // ���������� ��� (�)
	unsigned short T_pulse_code; // = 0x7409
	unsigned int T_pulse;        // ������������ �������� (���)
	unsigned short count_code;   // = 0xf4ff
	unsigned int count;          // ���������� ������������������ ���������
	unsigned short time_code;    // = 0x5300
	unsigned char hour;          // ����
	unsigned char minutes;       // ������
	unsigned char seconds;       // �������
	unsigned short date_code;    // = 0x6300
	unsigned char day;           // ����
	unsigned char month;         // �����
	unsigned char year;          // ���
	unsigned short signal_code;  // = 0x8100
	unsigned char signal;        // ������� ������� ("-56dBm" = 0xC8)
	unsigned short modem_code;   // = 0x9400
	unsigned intmodem_number;    // �������� ����� ������
	unsigned short error_code;   // = 0x010a
	unsigned char error;         // ������ (0x00 - ������ ���)
	unsigned char crc;           // ����������� ����� (CRC8)
	
	dataStruct()
	{
		head = 0x44;
		length = 0x34;
		device_code  = 0x1400;
		I_leack_code = 0x2407;
		I_pulse_code = 0x3407;
		T_pulse_code = 0x7409;
		count_code   = 0xf4ff;
		time_code    = 0x5300;
		date_code    = 0x6300;
		signal_code  = 0x8100;
		modem_code   = 0x9400;
		error_code   = 0x010a;
	}
	
} data;



//unsigned char data[10] = {2, 100};

// ================================================================

// ������ ����������� ����� CRC8
char getCRC8(char *str, int start, int length)
{
	char crc = 0xff;
	for (int j=start; j<length; j++)
	{
		crc = (char)(crc ^ str[j]);
		for (int i=0; i<8; i++)
		{
			if ((crc & 0x80) != 0x00)
			{
				crc = (char)(crc << 1);
				crc = (char)(crc ^ 0x31);
			}
			else
			{
				crc = (char)(crc << 1);
			}
		}
	}
	return crc;
}


// "���������" ������� ���������� ���� � �����
unsigned int reverse_bytes(unsigned int value)
{
	return (value & 0x000000FFU) << 24 | (value & 0x0000FF00U) << 8 |
				 (value & 0x00FF0000U) >> 8  | (value & 0xFF000000U) >> 24;
}

// ================================================================
//                         ������ RTOS
// ================================================================

static void uart_rx(void const *args);
osThreadDef(uart_rx, osPriorityNormal, 1, 2048);
static void uart_rx(void const *args)
{
	for (;;)
	{
		gpio.led('B', false);
		if (!uart.tx_ready) continue;
		uart.receive(1);
		uart.tx_ready = false;
		gpio.led('B', true);
		osDelay(100);	
	}
}


// ================================================================
//                         Main-�������
// ================================================================

int main(void)
{
	// ������������� ���������
	HAL_Init();
	gpio.init();
	uart.init();
	uart.receive(1);
	
	gpio.blink(3, 500);

	
	// ��������� ��������� ��������� ������
	data.device_number = reverse_bytes(2);
	data.I_leack 			 = reverse_bytes(2624);
	data.I_pulse 			 = reverse_bytes(38925);
	data.T_pulse 			 = reverse_bytes(768);
	data.count   			 = reverse_bytes(1);	
	
	// ������ ������� RTOS
	osThreadCreate (osThread(uart_rx), NULL);
	
	unsigned int I_arr[3] = {0, 1000, 100000};
	int I_state = 0;
	
	// �������� ���� (��������)
	for(;;)
	{
		HAL_IncTick();
		char state = gpio.key('A');
		gpio.led('R', state);
	
		
		
		if (state)
		{
			osDelay(10);

			int size = /*data.length + 4*/ sizeof(data);
			
			data.I_pulse = reverse_bytes(I_arr[I_state]);
			I_state++;
			if (I_state > 2) I_state = 0;
			
			data.count = reverse_bytes(data.count)+1;
			data.count = reverse_bytes(data.count);
			if (reverse_bytes(data.count) > 10) data.count = 0;
			while (gpio.key('A')) {}

			data.crc = getCRC8((char *)(&data), 0, size-1);				
			uart.send((char *)(&data), size);
		}
		
		
		//osSignalWait(0, osWaitForever);
	}
}
