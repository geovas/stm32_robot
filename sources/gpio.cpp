// =============================================
//        GPIO-���������� ��� STM32F303
//        �����: ������� �.�.
//        ������: v1.00 (��������)
// =============================================

// ---------------------------------------------
//            ����������� ���������
// ---------------------------------------------

#include <cmsis_os.h>				// ���������� ������ � RTOS Keil RTX
#include <stm32f3xx.h>			// �������� ���������� STM32F767
#include <stm32f3xx_hal.h>	// HAL-�������

#include "gpio.h"						// ���������� ������ � ������� �����-������

// ---------------------------------------------
//       ���������� ���������� � ��������
// ---------------------------------------------

gpio_class gpio;						// ������� ��������� ������


// ---------------------------------------------
//                  �������
// ---------------------------------------------

void gpio_class::init(void)	// ������������� ����� �����-������
{
	GPIO_InitTypeDef leds, keys, line;
	
	// ������� ���������
	GPIO_RED_START;
	leds.Pin		= GPIO_RED_PIN;
	leds.Speed	= GPIO_SPEED_FREQ_HIGH;
	leds.Mode		= GPIO_MODE_OUTPUT_PP;
	HAL_GPIO_Init(GPIO_RED_PORT, &leds);
	
	// ������� ���������
	GPIO_GREEN_START;
	leds.Pin		= GPIO_GREEN_PIN;
	HAL_GPIO_Init(GPIO_GREEN_PORT, &leds);

	// ������ ���������
	GPIO_BLUE_START;
	leds.Pin		= GPIO_BLUE_PIN;
	HAL_GPIO_Init(GPIO_BLUE_PORT, &leds);
	
	// -------------------------------------------
	
	// ������ A
	GPIO_KEY_A_START;
	keys.Pin		= GPIO_KEY_A_PIN;
	keys.Speed	= GPIO_SPEED_FREQ_HIGH;
	keys.Mode		= GPIO_MODE_INPUT;
	HAL_GPIO_Init(GPIO_KEY_A_PORT, &keys);
	
	// ����� "��"
	GPIO_LEFT_START;
	keys.Pin		= GPIO_LEFT_PIN;
	HAL_GPIO_Init(GPIO_LEFT_PORT, &keys);
	
	// ������ "��"
	GPIO_KEY_A_START;
	keys.Pin		= GPIO_RIGHT_PIN;
	HAL_GPIO_Init(GPIO_RIGHT_PORT, &keys);
	
	// -------------------------------------------
	
	// ����� �����������
	GPIO_LSERVO_START;
	line.Speed  = GPIO_SPEED_FREQ_HIGH;
	line.Pin		= GPIO_LSERVO_PIN;
	line.Mode		= GPIO_MODE_AF_PP;
	line.Alternate = GPIO_LSERVO_AF;
	HAL_GPIO_Init(GPIO_LSERVO_PORT, &line);
	
	// ������ �����������
	GPIO_RSERVO_START;
	line.Pin		= GPIO_RSERVO_PIN;
	line.Alternate = GPIO_RSERVO_AF;
	HAL_GPIO_Init(GPIO_RSERVO_PORT, &line);
	
	
	// ��������� ������� ��� ������ � ���
	TIM_HandleTypeDef htim;
	TIM_OC_InitTypeDef servo_left, servo_right;
	
	htim.Instance = GPIO_TIMER;
	htim.Init.Prescaler = 3;
	htim.Init.Period = 40000;
	htim.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim.Init.CounterMode = TIM_COUNTERMODE_UP;
	GPIO_TIMER_START;
	HAL_TIM_PWM_Init(&htim);

	// ��������� ������ �������, ������������� �� ����� �����������
	servo_left.Pulse = 0;
	servo_left.OCMode = TIM_OCMODE_PWM1;
	servo_left.OCPolarity = TIM_OCPOLARITY_HIGH;
	servo_left.OCFastMode = TIM_OCFAST_DISABLE;
	HAL_TIM_PWM_ConfigChannel(&htim, &servo_left, GPIO_LSERVO_CH);

	// ��������� ������ �������, ������������� �� ������ �����������
	servo_right.Pulse = 0;
	servo_right.OCMode = TIM_OCMODE_PWM1;
	servo_right.OCPolarity = TIM_OCPOLARITY_HIGH;
	servo_right.OCFastMode = TIM_OCFAST_DISABLE;
	HAL_TIM_PWM_ConfigChannel(&htim, &servo_right, GPIO_RSERVO_CH);

	// ������ ������� (� �����. �������������)
	HAL_TIM_PWM_Start(&htim, GPIO_LSERVO_CH);
	HAL_TIM_PWM_Start(&htim, GPIO_RSERVO_CH);
	
	return;
}

void gpio_class::led(char num, char state)	// ��������� ����������
{
	GPIO_PinState s = state?GPIO_PIN_SET:GPIO_PIN_RESET;
	
	switch (num)
	{
		case 1:
		case 'R':
			HAL_GPIO_WritePin(GPIO_RED_PORT, GPIO_RED_PIN, s);
		break;
		case 2:
		case 'G':
			HAL_GPIO_WritePin(GPIO_GREEN_PORT, GPIO_GREEN_PIN, s);
		break;
		case 3:
		case 'B':
			HAL_GPIO_WritePin(GPIO_BLUE_PORT, GPIO_BLUE_PIN, s);
		break;
	}
	
	return;
}

void gpio_class::blink(char count, int time)	// �������� ����� ������������ "count" ��� � ������������ "time" ��.
{
	int i;
	for (i=0; i<count; i++)
	{
		led(1, 1);
		led(2, 1);
		led(3, 1);
		osDelay(time);
		
		led(1, 0);
		led(2, 0);
		led(3, 0);
		osDelay(time);
	}
	
	return;
}


char gpio_class::key(char num)	// ������ ������
{
	GPIO_PinState state;
	
	switch(num)
	{
		case 0:
		case 'A':
			state = HAL_GPIO_ReadPin(GPIO_KEY_A_PORT, GPIO_KEY_A_PIN);
		break;
		
		case 1:
		case 'L':
			state = HAL_GPIO_ReadPin(GPIO_LEFT_PORT, GPIO_LEFT_PIN);
		break;
		
		case 2:
		case 'R':
			state = HAL_GPIO_ReadPin(GPIO_RIGHT_PORT, GPIO_RIGHT_PIN);
		break;
	}
	
	if (state == GPIO_PIN_SET) return 0;
	return 1;
	
}


void gpio_class::servo(char channel, int value)	// ��������� �������� (�� 0 �� 100%) PWM �����������
{
	
	
	switch (channel)
	{
		case 0:
		case 'R':
			// value �.�. �� 2000 �� 4000 (�����. 1�� - 2��)
			value = 2000 + (100-value) * 20;
			GPIO_TIMER->GPIO_RSERVO_CC = value;
		break;
		
		case 1:
		case 'L':
			// value �.�. �� 4000 �� 2000 (�����. 1�� - 2��)
			value = 2000 + value * 20;
			GPIO_TIMER->GPIO_LSERVO_CC = value;	
		break;		
	}
	
	return;
}


// ---------------------------------------------
